# AndroidRSS



## Getting started

To make it easy for you to get started with this project, here is a list of recommended steps.

## Clone The Repo

clone the repository using this command:

```
cd clone https://gitlab.com/hanifhefaz/androidrss.git
```

## Build and sync your project

after clone, open the project in Android Studio and click on file menu. Now select ```Sync Project with Gradle Files```

## Connect a Mobile device and RUN

insert your mobile device and enable developer options on your device. then run the project.

## Add new RSS Link

Use the built-in continuous integration in GitLab.

in order to add new link, since the project links are hard coded, you should these steps:

- Insert a button in your ```activity_home.xml```

- Insert an empty Activity with resource layout

- Copy the code from MainActivity to your new Activity

- Change the Class name to your Activity name.

- Change the URL to your RSS URL.

- Copy the code from activity_home.xml to your new resource layout file, and change the activity name there.

- Run the project, and look for errors if there are some.

That is it!
