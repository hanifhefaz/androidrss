package com.example.myapplication;

public class NewsInformation
{
    public Integer id;
    public String link;
    public String title;
    public String pubDate;
    public String description;
    public String image;
    public String category;

}
