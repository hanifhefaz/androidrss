package com.example.myapplication.OCS;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.AOP.DatabaseHandlerAOP;
import com.example.myapplication.NewsInformation;
import com.example.myapplication.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class CustomAdapterOCS extends ArrayAdapter<NewsInformation> {

    private DatabaseHandlerOCS db;
    public CustomAdapterOCS(Context context, ArrayList<NewsInformation> news) {

        super(context, 0, news);
        db = new DatabaseHandlerOCS(getContext());
    }


    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        NewsInformation item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }



        TextView title = (TextView) convertView.findViewById(R.id.text1);
        title.setText(item.title);

        TextView description = (TextView) convertView.findViewById(R.id.text2);
        description.setText(item.description);

        Button btShowmore = (Button) convertView.findViewById(R.id.showButton);
        btShowmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btShowmore.getText().toString().equalsIgnoreCase("More..."))
                {
                    description.setMaxLines(Integer.MAX_VALUE);//your TextView
                    btShowmore.setText("Less...");
                }
                else
                {
                    description.setMaxLines(3);//your TextView
                    btShowmore.setText("More...");
                }
            }
        });


        TextView pubDate = (TextView) convertView.findViewById(R.id.text3);
        pubDate.setText(item.pubDate);

        String imageUri = item.image;
        ImageView ivBasicImage = (ImageView) convertView.findViewById(R.id.image);

        if(isNetworkAvailable(getContext())) {
            if (imageUri.isEmpty()) {
                Picasso.get().load("https://aop.gov.af/storage/uploads/news/1001_t.jpg").into(ivBasicImage);
            } else {
                Picasso.get().load(imageUri).into(ivBasicImage);
            }
        }
        else
        {
            Picasso.get().load(imageUri).into(ivBasicImage);
        }

        Button checkButton = (Button) convertView.findViewById(R.id.checkButton);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.remove(item.title);
                remove(item);
                Toast toast = Toast.makeText(getContext(), "Deleted!", Toast.LENGTH_SHORT);
                toast.show();
            }

        });

        TextView category = (TextView) convertView.findViewById(R.id.text4);
        category.setText(item.category);



        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Uri uri = Uri.parse(item.link);
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    getContext().startActivity(intent);

            }
        });
        return convertView;

    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


}
