package com.example.myapplication.OCS;

import static android.content.ContentValues.TAG;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.myapplication.NewsInformation;
import com.example.myapplication.WebsitesInformation;

import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseHandlerOCS {

    private DatabaseHelperOCS dbHelper;
    private SQLiteDatabase database;


    public DatabaseHandlerOCS(Context context) {
        dbHelper = new DatabaseHelperOCS(context);
    }

    //methods for all table
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    //news table method

    public void insertNewsInfo(NewsInformation newsInfo) {
        ContentValues cv = new ContentValues();
        cv.put("link"          ,  newsInfo.link );
        cv.put("title"           ,  newsInfo.title );
        cv.put("pubDate"        ,  newsInfo.pubDate );
        cv.put("description"        ,  newsInfo.description );
        cv.put("image"        ,  newsInfo.image );
        cv.put("category"        ,  newsInfo.category );

        database.insertWithOnConflict("news", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }


    public ArrayList<NewsInformation> getAllNews() {
        ArrayList<NewsInformation> NewsInfoList = new ArrayList<NewsInformation>();

        Cursor cursor = database.rawQuery("SELECT DISTINCT title, description, pubDate, category FROM  news ORDER BY pubDate DESC" ,new String[]{});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            NewsInformation newsInfo = new NewsInformation();

//            newsInfo.link = cursor.getString(0);
            newsInfo.title = cursor.getString(0);
            newsInfo.description = cursor.getString(1);
            newsInfo.pubDate = cursor.getString(2);
            newsInfo.category = cursor.getString(3);
//            newsInfo.image = cursor.getString(4);

            NewsInfoList.add(newsInfo);
            cursor.moveToNext();
        }

        // Make sure to close the cursor
        cursor.close();

        return NewsInfoList;
    }

    public void remove(String title)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("news", "title=?", new String[]{title});
        db.close();

    }

    public void update(NewsInformation newsInfo)
    {
        ContentValues cv = new ContentValues();
        cv.put("link"          ,  newsInfo.link );
        cv.put("title"           ,  newsInfo.title );
        cv.put("pubDate"        ,  newsInfo.pubDate );
        cv.put("description"        ,  newsInfo.description );
        cv.put("image"        ,  newsInfo.image );
        cv.put("category"        ,  newsInfo.category );

        database.update("news", cv, "title=?", new String[]{newsInfo.title} );
    }

    public boolean hasObject(String title) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String selectString = "SELECT * FROM news" + " WHERE title " + " =?";

        Cursor cursor = db.rawQuery(selectString, new String[] {title});

        boolean hasObject = false;
        if(cursor.moveToFirst()){
            hasObject = true;

            //region if you had multiple records to check for, use this region.

            int count = 0;
            while(cursor.moveToNext()){
                count++;
            }
            //here, count is records found
            Log.d(TAG, String.format("%d records found", count));

            //endregion

        }

        cursor.close();          // Dont forget to close your cursor
        return hasObject;
    }

}
